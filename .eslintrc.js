// eslint-disable-next-line @typescript-eslint/no-var-requires
const eslint = { ...require('@cai/configs').eslint }
eslint.extends.push('react-app')

eslint.env['cypress/globals'] = true
eslint.extends.push('plugin:cypress/recommended')
eslint.parserOptions.project.push('./cypress/tsconfig.json')
eslint.plugins.push('cypress')
eslint.rules['cypress/no-assigning-return-values'] = 'error'
eslint.rules['cypress/no-unnecessary-waiting'] = 'error'
eslint.rules['cypress/assertion-before-screenshot'] = 'error'

module.exports = eslint

// Above is the @CAI-config package, below is the local eslint rule-set
// The below local linting rules can be removed in the future

// module.exports = {
//   root: true,
//   env: {
//     browser: true,
//     es6: true,
//     node: true,
//     'cypress/globals': true,
//   },
//   extends: [
//     'eslint:recommended',
//     'plugin:@typescript-eslint/eslint-recommended',
//     'plugin:@typescript-eslint/recommended',
//     'plugin:cypress/recommended',
//     'plugin:prettier/recommended',
//     'react-app',
//   ],
//   parser: '@typescript-eslint/parser',
//   parserOptions: {
//     createDefaultProgram: true,
//     ecmaFeatures: {
//       jsx: true,
//       modules: true,
//     },
//     project: ['tsconfig.json', './cypress/tsconfig.json'],
//     sourceType: 'module',
//   },
//   overrides: [
//     {
//       // overrides for Javascript files
//       files: ['*.js'],
//       rules: {
//         '@typescript-eslint/explicit-function-return-type': 'off',
//       },
//     },
//     {
//       // overrides for TypeScript files
//       files: ['*.ts', '*.tsx'],
//       rules: {
//         '@typescript-eslint/explicit-member-accessibility': ['error'],
//       },
//     },
//   ],
//   plugins: [
//     '@typescript-eslint',
//     'cypress',
//     'import', // eslint-plugin-*  prefix can be ommitted
//     'prefer-arrow',
//     'prettier',
//     'react',
//   ],
//   settings: {
//     'import/parsers': {
//       '@typescript-eslint/parser': ['.ts', '.tsx'],
//     },
//     'import/resolver': {
//       node: {
//         extensions: ['.js', '.jsx', '.ts', '.tsx'],
//       },
//       // use <root>/tsconfig.json
//       typescript: {
//         // always try to resolve types under `<root>@types` directory
//         // even it doesn't contain any source code, like `@types/unist`
//         alwaysTryTypes: true,
//       },
//     },
//     react: {
//       version: 'detect',
//     },
//   },
//   rules: {
//     '@typescript-eslint/adjacent-overload-signatures': 'error',
//     '@typescript-eslint/array-type': 'error',
//     '@typescript-eslint/await-thenable': 'error',
//     '@typescript-eslint/ban-types': 'error',
//     '@typescript-eslint/consistent-type-assertions': 'error',
//     '@typescript-eslint/explicit-member-accessibility': [
//       'error',
//       {
//         overrides: {
//           constructors: 'off',
//         },
//       },
//     ],
//     '@typescript-eslint/explicit-module-boundary-types': 'warn',
//     '@typescript-eslint/indent': 'off',
//     '@typescript-eslint/interface-name-prefix': 'off',
//     '@typescript-eslint/member-delimiter-style': [
//       'error',
//       {
//         multiline: {
//           delimiter: 'none',
//           requireLast: true,
//         },
//         singleline: {
//           delimiter: 'semi',
//           requireLast: false,
//         },
//       },
//     ],
//     '@typescript-eslint/member-ordering': 'error',
//     '@typescript-eslint/no-empty-function': 'error',
//     '@typescript-eslint/no-empty-interface': 'error',
//     '@typescript-eslint/no-explicit-any': 'off',
//     '@typescript-eslint/no-misused-new': 'error',
//     '@typescript-eslint/no-namespace': 'error',
//     '@typescript-eslint/no-parameter-properties': 'off',
//     '@typescript-eslint/no-redeclare': 'error',
//     '@typescript-eslint/no-unused-vars': 'warn',
//     '@typescript-eslint/no-use-before-define': 'off', // 'React' was used before it was defined
//     '@typescript-eslint/no-var-requires': 'error',
//     '@typescript-eslint/prefer-for-of': 'error',
//     '@typescript-eslint/prefer-function-type': 'error',
//     '@typescript-eslint/prefer-namespace-keyword': 'error',
//     '@typescript-eslint/prefer-regexp-exec': 'warn',
//     '@typescript-eslint/quotes': ['error', 'single'],
//     '@typescript-eslint/require-await': 'warn',
//     '@typescript-eslint/semi': ['error', 'never'],
//     '@typescript-eslint/triple-slash-reference': 'warn',
//     '@typescript-eslint/type-annotation-spacing': 'error',
//     '@typescript-eslint/unbound-method': [
//       'warn',
//       {
//         ignoreStatic: true,
//       },
//     ],
//     '@typescript-eslint/unified-signatures': 'error',
//     'arrow-body-style': 'warn',
//     'arrow-parens': ['off', 'as-needed'],
//     complexity: 'off',
//     'constructor-super': 'error',
//     curly: ['error', 'multi-line'],
//     'dot-notation': 'error',
//     'eol-last': 'error',
//     'cypress/no-assigning-return-values': 'error',
//     'cypress/no-unnecessary-waiting': 'error',
//     'cypress/assertion-before-screenshot': 'warn',
//     eqeqeq: ['error', 'smart'],
//     'guard-for-in': 'error',
//     'id-blacklist': [
//       'warn',
//       'any',
//       'Boolean',
//       'boolean',
//       // 'callback',
//       // 'cb',
//       // 'data',
//       // 'e',
//       // 'err',
//       'number',
//       'Number',
//       'String',
//       'string',
//       'undefined',
//       'Undefined',
//     ],
//     'id-match': 'error',
//     'import/no-unresolved': 'error',
//     'import/order': 'off',
//     'max-classes-per-file': ['error', 1],
//     'max-len': [
//       'warn',
//       {
//         code: 110,
//         ignoreComments: true,
//         ignoreRegExpLiterals: true,
//         ignoreStrings: true,
//         ignoreTemplateLiterals: true,
//         ignoreTrailingComments: true,
//         ignoreUrls: true,
//       },
//     ],
//     'new-parens': 'error',
//     'no-bitwise': 'error',
//     'no-caller': 'error',
//     'no-cond-assign': 'error',
//     'no-console': 'off',
//     // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
//     'no-debugger': 'error',
//     'no-empty': 'error',
//     'no-eval': 'error',
//     'no-fallthrough': 'off',
//     'no-invalid-this': 'off',
//     'no-multiple-empty-lines': 'error',
//     'no-new-wrappers': 'error',
//     'no-shadow': [
//       'warn',
//       {
//         builtinGlobals: false,
//         hoist: 'functions',
//       },
//     ],
//     'no-throw-literal': 'error',
//     'no-trailing-spaces': 'error',
//     'no-undef-init': 'error',
//     'no-unsafe-finally': 'error',
//     'no-unused-expressions': 'error',
//     'no-unused-labels': 'error',
//     'no-unused-vars': 'off',
//     'no-use-before-define': 'off',
//     'no-var': 'error',
//     'object-shorthand': 'error',
//     'prefer-arrow/prefer-arrow-functions': 'warn',
//     'prefer-const': 'error',
//     'prefer-object-spread': 'error',
//     'prettier/prettier': 'error',
//     'quote-props': ['error', 'as-needed'],
//     radix: 'error',
//     'react/display-name': 'warn',
//     'react/jsx-uses-react': 'error',
//     'react/jsx-uses-vars': 'error',
//     'react/prop-types': 'off',
//     'react-hooks/exhaustive-deps': 'off',
//     'space-before-function-paren': [
//       'error',
//       {
//         anonymous: 'always',
//         asyncArrow: 'always',
//         named: 'never',
//       },
//     ],
//     'use-isnan': 'error',
//     'valid-typeof': 'off',
//   },
// }
