import { SnackbarManager } from '@rfh/ui'
import React from 'react'

import {
  initializePushNotificationsAsync,
  onPushNotificationGeneric,
} from './../../common/services/pushNotificationService'

export const PushNotificationProvider: React.FC = ({ children }) => {
  React.useEffect(() => {
    initializePushNotificationsAsync()
    onPushNotificationGeneric(message => {
      SnackbarManager.show(
        <>
          <h4>{message.title}</h4>
          {message.body}
        </>
      )
    })
  }, [])

  return <>{children}</>
}
