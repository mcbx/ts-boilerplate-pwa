import { FC, ReactElement } from 'react'

import Skeleton from '@mui/lab/Skeleton'

export const TableRowMock: FC = (): ReactElement => (
  <tr>
    <td>
      <Skeleton variant='text' />
    </td>
    <td>
      <Skeleton variant='text' />
    </td>
    <td>
      <Skeleton variant='text' />
    </td>
    <td>
      <Skeleton variant='text' />
    </td>
    <td>
      <Skeleton variant='text' />
    </td>
    <td>
      <Skeleton variant='text' />
    </td>
    <td>
      <Skeleton variant='text' />
    </td>
  </tr>
)
