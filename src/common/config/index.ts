import { Config } from './Config'
import { ThemeConfig } from './ThemeConfig'

export { Config, ThemeConfig }
