import { IConfig } from 'src/common/interfaces'
import firebaseConfig from './firebase.json'

// These values {#...#} are replaced in the release pipeline
// If they're not replaced then the local test values are used
let envValue = '#{OktaOmgeving}#'
if (envValue.startsWith('#{')) {
  envValue = 'dev'
}
console.log('Env: ' + envValue)

let apiHostValue = '#{ApiHost}#'
if (apiHostValue.startsWith('#{')) {
  apiHostValue = 'http://localhost:4000'
}

// Enable or disable OktaAuthentication (IE: disable for Cypress testing)
let oktaAuthentication = '#{OktaAuthentication}#'
if (oktaAuthentication.startsWith('#{')) {
  oktaAuthentication = 'on'
}

let redirectUriValue = '#{OktaRedirectUri}#'
if (redirectUriValue.startsWith('#{')) {
  redirectUriValue = 'https://localhost:3000/callback'
}

let clientIdValue = '#{OktaClientId}#'
if (clientIdValue.startsWith('#{') || clientIdValue === '') {
  clientIdValue = '0oa1jefrs0g9vW2sv0x7' //tst-boilerplate-dev
}

let domainValue = '#{OktaDomain}#'
if (domainValue.startsWith('#{')) {
  domainValue = 'https://identity-test.royalfloraholland.com/oauth2/default'
}

let issuerValue = '#{OktaIssuer}#'
if (issuerValue.startsWith('#{')) {
  issuerValue = 'https://identity-test.royalfloraholland.com/oauth2/default'
}

export const Config: IConfig = {
  env: envValue,
  oktaRoute: oktaAuthentication,
  api: {
    host: apiHostValue,
    path: '',
    version: '',
  },
  oidc: {
    clientId: clientIdValue,
    domain: domainValue,
    issuer: issuerValue,
    redirectUri: redirectUriValue,
    scopes: ['openid', 'profile', 'email', 'tst-boilerplate-api-' + envValue],
    pkce: true,
  },
  aws: {
    IdentityPoolId: 'eu-west-1:34b2b6f1-3c5c-4263-84ec-ba688f577da4',
    region: 'eu-west-1',
    PlatformApplicationArn:
      'arn:aws:sns:eu-west-1:002779451522:app/GCM/pnb-oi-subscriber-sns-platform-app',
    topicArn: 'arn:aws:sns:eu-west-1:002779451522:pnb-oi-notifications-topic',
    dotnetApiSubscribe:
      'https://pnb-oi-notifications-api.sandbox.royalfloraholland.com/subscriber',
    dotnetApiPublish:
      'https://pnb-oi-notifications-api.sandbox.royalfloraholland.com/publisher',
  },
  firebase: firebaseConfig,
}
