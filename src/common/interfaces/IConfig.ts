export interface IConfig {
  env: string
  oktaRoute: string
  api: {
    host: string
    path: string
    version: string
  }
  oidc: {
    clientId: string
    domain: string
    issuer: string
    redirectUri: string
    scopes: string[]
    pkce: boolean
  }
  aws: {
    IdentityPoolId: string
    region: string
    PlatformApplicationArn: string
    topicArn: string
    dotnetApiSubscribe: string
    dotnetApiPublish: string
  }
  firebase: {
    apiKey: string
    authDomain: string
    projectId: string
    storageBucket: string
    messagingSenderId: string
    appId: string
  }
}
