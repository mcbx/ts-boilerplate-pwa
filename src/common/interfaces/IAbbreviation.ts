export interface IAbbreviation {
  id: string
  abbreviation: string
  description: string
  englishDescription: string
  description2: string
  created: string
  createdBy: string
  updated: string
  updatedBy: string
  version: number
}
