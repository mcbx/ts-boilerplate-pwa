import { useContext } from 'react'
import { ConfirmationProviderContext } from '../providers/ConfirmationProvider'
import { ConfirmationOptions } from '../components/ConfirmationDialog'

type curryType = (options: ConfirmationOptions) => Promise<any>
export const useConfirmation = (): curryType =>
  useContext(ConfirmationProviderContext)
