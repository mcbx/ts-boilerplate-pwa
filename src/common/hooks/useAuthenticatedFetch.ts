/* eslint-disable no-console */
import { useState, useEffect } from 'react'
import { useOktaAuth } from '@okta/okta-react'
import { getAbbreviations } from 'src/common/services'
import { IAbbreviation } from 'src/common/interfaces'

// Example of authenticated data fetching with a custom hook
export const useGetAbbreviations = (): IAbbreviation[] => {
  const { authState } = useOktaAuth()
  const [token] = useState(authState.accessToken?.accessToken)
  const [response, setResponse] = useState<IAbbreviation[]>([])

  const fetchData = async (): Promise<void> => {
    try {
      const result = await getAbbreviations(token)
      if (result) {
        setResponse(result)
      }
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [token])
  return response
}
