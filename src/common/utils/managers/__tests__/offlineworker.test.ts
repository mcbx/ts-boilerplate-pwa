import {
  addLocalItem,
  getLocalItem,
  removeLocalItem,
  setLocalItem,
  updateLocalItem,
} from '../OfflineManager'

let actual
let expected
let testId

test('test_adding_item_succesfull', (): void => {
  testId = 'first_item'
  expected = 'first_item_added'
  addLocalItem<string>(testId, expected)

  actual = getLocalItem<string>(testId)
  expect(actual).toStrictEqual([expected])
})

test('removing_item_succesfull', (): void => {
  testId = 'second_item'
  expected = 'second_item_added'
  addLocalItem<string>(testId, 'second_item_added')

  actual = getLocalItem<string>(testId)
  expect(actual).toStrictEqual([expected])

  removeLocalItem(testId)
  expect(actual).toStrictEqual([expected])

  actual = getLocalItem<string>(testId)
  expect(actual).toStrictEqual([])
})

test('test_setting_item_succesfull', (): void => {
  testId = 'second_item'
  expected = 'first_item_set'
  setLocalItem<string>(testId, expected)

  actual = getLocalItem<string>(testId)
  expect(actual).toStrictEqual(expected)
})

test('updating_item_succesfull', (): void => {
  testId = 'second_item'
  expected = 'second_item_updated'

  setLocalItem<string>(testId, 'someId')
  updateLocalItem(testId, expected)

  actual = getLocalItem<string>(testId)
  expect(actual).toBe(expected)
})
