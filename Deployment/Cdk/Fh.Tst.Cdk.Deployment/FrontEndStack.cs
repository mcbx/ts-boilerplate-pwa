using Amazon.CDK;
using Amazon.CDK.AWS.CloudFront;
using Amazon.CDK.AWS.Route53;
using Amazon.CDK.AWS.S3;
using Fh.Tst.Cdk.Deployment.Environments;
using RfhCdk;
using RfhCdk.Services.CloudFront;
using RfhCdk.Services.CustomResource.Okta;
using RfhCdk.Services.Route53;
using RfhCdk.Services.S3;
using RfhCdk.Services.Ssm;
using System.Linq;

namespace Fh.Tst.Cdk.Deployment
{
    public class FrontEndStack : Stack
    {
        private readonly ICustomEnvironment environment;

        public FrontEndStack(Construct scope, string id, IStackProps props, ICustomEnvironment environment) : base(scope, id, props)
        {
            this.environment = environment;

            var bucket = CreateS3Bucket();
            var distribution = CreateCloudFrontDistribution(bucket);
            var record = CreateRoute53Record(distribution);
            _ = CreateOktaAppRegistration(record.DomainName);
        }

        private IBucket CreateS3Bucket()
        {
            var context = new RfhResourceContext(this, environment, "pwa-frontend");

            var bucket = new RfhS3BucketFactory(context).CreateCloudFrontBucket();

            return bucket;
        }

        private IDistribution CreateCloudFrontDistribution(IBucket bucket)
        {
            var context = new RfhResourceContext(this, environment, "pwa-frontend");

            var webAclArnToken = new RfhSsmParameterProvider(context).GetWafCloudfrontAkamaiWebAclArnToken();

            return new RfhCloudFrontDistributionFactory(context).Create(bucket, webAclArn: webAclArnToken)
                                                                .AddDistributionIdOutput(context);
        }

        private ARecord CreateRoute53Record(IDistribution distribution)
        {
            var context = new RfhResourceContext(this, environment, "pwa-frontend");

            return new RfhRecordFactory(context).CreateCloudFrontRecord(distribution);
        }

        private RfhOktaAppRegistration CreateOktaAppRegistration(string websiteDomainName)
        {
            var context = new RfhResourceContext(this, environment, "pwa-frontend");

            var parameters = new RfhOAuthAppRegistrationParams
            {
                PrettyApplicationName = "boilerplate",
                CustomerRoles = new string[0],
                EmployeeRoles = new RfhOktaEmployeeRole[]
                {
                  new RfhOktaEmployeeRole("Medewerker", RfhOktaEmployeeRoleMembers.EmployeesInApplicationGroup),
                  new RfhOktaEmployeeRole("Manager", RfhOktaEmployeeRoleMembers.EmployeesInApplicationGroup),
                  new RfhOktaEmployeeRole("AlleenLezen", RfhOktaEmployeeRoleMembers.AllEmployees), // add this only if all employees must be able to access the application
                },
                RedirectUris = new[] { $"https://{websiteDomainName}/callback" }.Concat(environment.AdditionalWebsiteRedirectUris).ToArray(),
                PostLogoutRedirectUris = new[] { $"https://{websiteDomainName}" }.Concat(environment.AdditionalWebsiteSignOutUris).ToArray(),
            };

            return new RfhOktaAppRegistrationFactory(context).CreateOAuthAppRegistration(parameters)
                                                             .AddClientIdOutput(context);
        }
    }
}
