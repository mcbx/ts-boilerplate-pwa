using RfhAwsInfo;
using RfhCdk;
using System.Collections.Generic;

namespace Fh.Tst.Cdk.Deployment.Environments
{
    public class EnvironmentDev : RfhEnvironment, ICustomEnvironment
    {
        public EnvironmentDev(string systemCode, RfhDomain domain, RfhAwsAccount awsAccount, RfhCicdPipeline pipeline)
            : base(systemCode, domain, awsAccount, pipeline, new Dictionary<string, string>())
        {}

        public string[] AdditionalWebsiteRedirectUris => new[] { "https://tst-dev.royalfloraholland.com/callback","https://localhost:3000/implicit/callback", "https://localhost:3000/callback", "https://localhost:3000/callbackfortest" };
        public string[] AdditionalWebsiteSignOutUris => new[] { "https://tst-dev.royalfloraholland.com","https://localhost:3000" };
    }
}
