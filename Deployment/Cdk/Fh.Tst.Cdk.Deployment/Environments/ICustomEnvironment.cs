using RfhCdk;

namespace Fh.Tst.Cdk.Deployment.Environments
{
    /// <summary>
    /// Put all you custom setups for constructs needed by you stacks
    /// </summary>
    public interface ICustomEnvironment : IRfhEnvironment
    {
        string[] AdditionalWebsiteRedirectUris { get; }
        string[] AdditionalWebsiteSignOutUris { get; }
    }
}
