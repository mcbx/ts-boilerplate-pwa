using Amazon.CDK;
using Fh.Tst.Cdk.Deployment.Environments;
using RfhAwsInfo;
using RfhCdk;
using RfhCdk.Tagging;
using System;
using System.Collections.Generic;

namespace Fh.Tst.Cdk.Deployment
{
    class Program
    {
        private static App app;
        
        public static void Main(string[] args)
        {
            app = new App();

            var environment = SetupEnvironment(app);

            CreateServiceStack(environment, app);

            app.Synth();
        }

        private static ICustomEnvironment SetupEnvironment(Construct app)
        {
            var pipeline = new RfhCicdPipeline(app.Node);

            ICustomEnvironment environment;

            switch (pipeline.Stage.StageName)
            {
                case RfhStage.StageName_Dev:
                    environment = new EnvironmentDev("Tst", RfhDomains.Shared, RfhAwsAccounts.rfh_sandbox, pipeline);
                    break;
                //case RfhStage.StageName_Sys:
                //    environment = new EnvironmentSys("Tst", RfhDomains.Shared, RfhAwsAccounts.rfh_shared_staging, settings);
                //    break;
                //case RfhStage.StageName_Acc:
                //    environment = new EnvironmentAcc("Tst", RfhDomains.Shared, RfhAwsAccounts.rfh_shared_staging, settings);
                //    break;
                default:
                    throw new Exception("Unknown stage");
            }
            return environment;
        }

        private static void CreateServiceStack(ICustomEnvironment environment, App app)
        {
            var props = new StackProps
            {
                Env = environment.AwsEnvironment,
                StackName = environment.NameComposer.GetResourceName("pwa-frontend", RfhResourceType.CloudFormationStack),
                Description = $"Stack for the application: {environment.SystemCode}"
            };
            Console.WriteLine($"Created stack: {props.StackName}");

            var stack = new FrontEndStack(app, $"{nameof(FrontEndStack)}-{environment.Stage.StageName}", props, environment);

            stack.AddTags(environment.SystemCode, RfhSlaLevels.Bronze, RfhTopdeskTeams.CloudOps, environment.Stage.StageName);
        }
    }
}
