# Internationalization

[react-i18next](https://react.i18next.com/) is a powerful internationalization framework for React / React Native which is based on [i18next](https://www.i18next.com/).

Install the necessary libraries and create the translation folder:

```none
npm i i18next i18next-xhr-backend react-i18next -S
mkdir -p public/assets/i18n/translations
```

For now, we have put the translations to _static json files_. Later, we can put them in a _Rest endpoint_.
