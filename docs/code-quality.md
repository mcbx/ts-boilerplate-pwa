# Code Quality and Style Guide

1. [Code Quality and Style Guide](#code-quality-and-style-guide)
   1. [Linting and Code Formatting](#linting-and-code-formatting)
      1. [General rules](#general-rules)
      1. [Linting with ESLint instead of TSLint](#linting-with-eslint-instead-of-tslint)
      1. [Code Formatting](#code-formatting)
   1. [Tools and Techniques](#tools-and-techniques)
      1. [Developer Environment](#developer-environment)
      1. [Design](#design)
      1. [React](#react)
      1. [Authorization](#authorization)
      1. [CI/CD](#cicd)

> This document is WIP (Work in Progress). Feel free to comment on it.

By CAI, we want to be consistent when developing our front-end applications. Therefore, it is very important that all devs, new and old, inexeperienced and experienced, should follow these guidelines.

## Linting and Code Formatting

### General rules

The following rules are listed here and they are also enforced by the linting tools we use like `eslint` and `prettier` (see below).

- Use single quotes in Javascript code, not double quotes
- Avoid semicolons at the end of statements
- Avoid declaring variables that are not used in the same module
- Use relative links in imports (`src/` notation). Use the following settings in `tsconfig.json`:
  ```json
  "baseUrl": ".",
  "exclude": [
      "build",
      "node_modules"
  ],
  "include": [
      "src"
  ]
  ```
- Sort imports

### Linting with ESLint instead of TSLint

As stated in [TSLint in 2019](https://medium.com/palantir/tslint-in-2019-1a144c2317a9):

> In order to avoid bifurcating the linting tool space for TypeScript, we therefore plan to deprecate TSLint and focus our efforts instead on improving ESLint’s TypeScript support.

Moreover, the team CRA (Create React App) has already included TS linting with ESLint out of the box in its [Roadmap for version 3.0](https://github.com/facebook/create-react-app/issues/6475). And we are currently at CRA version 3.2.

Therefore, we will also be using ESLint in all our TypeScript projects.

```bash
npm i -D @types/react @typescript-eslint/eslint-plugin @typescript-eslint/parser eslint-config-prettier eslint-config-react eslint-plugin-prettier prettier
```

Add two files in the root of the React project next to `tsconfig.json`, `.prettierrc`, `.eslintrc.js` and `.eslintignore`.

### Code Formatting

1. keep default formatter for `JS, JSX, TS, TSX` files as `Prettier - Code formatter`
1. run formatting in VS Code by <kbd>Shift</kbd>+<kbd>Alt</kbd>+<kbd>F</kbd>
1. if you see that TypeScript imports are not sorted (some lines are underlined by red squiggles), right click the file surface somewhere, choose `Source Action...` and then `Fix All TSLint` option if it is available<br/>
   > Be carefull not to choose the `Organize Imports` option as it does not fully observe the linting settigs. If you click it by mistake and it breaks the formatting, just start all over again by step 2.

## Tools and Techniques

### Developer Environment

- Use VSCode with the minimum required extensions (see pre-requisites document)
- Use mock backend: `json-server` or Docker container(s) (e.g., combination of API and PosgreSQL containers)
- Dev environment in a Docker container based on Microsoft `Remote - Containers` extension (?)

### Design

- `@rfh/ui` NPM package for UI-componenten
- `Material-UI` if not enough

### React

- Use React Function Components and React Hooks; avoid React Class Components
- Code structure
  - src
    - common
      - components
        - component name
      - config
      - enums
      - hooks
      - interfaces
      - providers
      - services
      - utils
    - modules
      - module name
        - views
          - components
            - component name
- Every folder contains `index.ts` file with all imports and exports from all `*.tsx` files inside the folder
- Unit testing `react-testing-library` (Jest, Mocha)
- UI testing met `Cypress`
- State management (Context API, MobX, Redux met hooks)
- Component promotion procedure
- `i18n` for multi-language
- Offline manager (as a Service Worker)
- React syntactic sugar
  - <React.Fragment> ... </React.Fragment> => <> ... </>
  -

### Authorization

- OKTA
- Authorizer Lambda

### CI/CD

- These steps should be present in the Build pipeline:
  - Caching of `node_modules` folder based on `package-lock.json` file hash
  - Linting
  - Unit tests with code coverage
  - SonarCloud check
- Release pipeline
  - deployment to CloudFront
  - smoke testing
  - UI testing
