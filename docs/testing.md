# Testing

## Unit testing with JEST

We are seriously considering to drop JEST completely and do all tests via CYPRESS. It seems that snapshot testing as a type of tests adds little value and much hassle. Better to concentrate all efforts on UI and integration tests. Cypress is very good at that.

### Focusing and Excluding Tests

You can replace `it()` with `xit()` to temporarily exclude a test from being executed. Similarly, `fit()` lets you focus on a specific test without running any other tests.

### Disabling jsdom

By default, the `package.json` of the generated project looks like this:

```js
"scripts": {
  "start": "react-scripts start",
  "build": "react-scripts build",
  "test": "react-scripts test --env=jsdom"
```

If you know that none of your tests depend on [jsdom](https://github.com/tmpvar/jsdom), you can safely remove `--env=jsdom`, and your tests will run faster:

```diff
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
-   "test": "react-scripts test --env=jsdom"
+   "test": "react-scripts test"
```

To help you make up your mind, here is a list of APIs that **need jsdom**:

- Any browser globals like `window` and `document`
- [`ReactDOM.render()`](https://facebook.github.io/react/docs/top-level-api.html#reactdom.render)
- [`TestUtils.renderIntoDocument()`](https://facebook.github.io/react/docs/test-utils.html#renderintodocument) ([a shortcut](https://github.com/facebook/react/blob/34761cf9a252964abfaab6faf74d473ad95d1f21/src/test/ReactTestUtils.js#L83-L91) for the above)
- [`mount()`](http://airbnb.io/enzyme/docs/api/mount.html) in [Enzyme](http://airbnb.io/enzyme/index.html)

In contrast, **jsdom is not needed** for the following APIs:

- [`TestUtils.createRenderer()`](https://facebook.github.io/react/docs/test-utils.html#shallow-rendering) (shallow rendering)
- [`shallow()`](http://airbnb.io/enzyme/docs/api/shallow.html) in [Enzyme](http://airbnb.io/enzyme/index.html)

Finally, jsdom is also not needed for [snapshot testing](http://facebook.github.io/jest/blog/2016/07/27/jest-14.html).

## UI testing with CYPRESS

We are seriously considering to drop **JEST** completely and do all tests via **CYPRESS**. It seems that snapshot testing as a type of tests adds little value and much hassle. Better to concentrate all efforts on UI and integration tests. Cypress is very good at that.

### localStorage

We are using the [i18next-browser-languageDetector](https://github.com/i18next/i18next-browser-languageDetector) plugin. This plugin uses the `i18nextLng` key:

```ts
const setLanguage = (lang: string): void => {
  localStorage.setItem('i18nextLng', lang)
}
```

### More Cypress Examples

- [TODO MVC](https://github.com/cypress-io/cypress-example-todomvc)
- [Kitchensink](https://github.com/cypress-io/cypress-example-kitchensink)
- [Writing Your First Test](https://on.cypress.io/writing-your-first-test)
