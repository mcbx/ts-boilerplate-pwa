# OKTA Authentication and Authorization

> This section should be rewritten after we have implemented the new way of Okta authorization.

## OKTA Portal

1. In order to set up the OKTA authentication and authorisation, somebody with admin access should go to the [RFH Test Okta](https:/identity-test.royalfloraholland.com/app/UserHome) portal and log in as the admin user.

   ![okta admin](images/okta_admin.png)

1. Create the necessary user(s) via `Directory | People`, e.g. `Test React Conclusion` user.

1. Create a new application for the project in `Applications | Applications`, e.g., `CAI Base React`.

1. Click on your new application, go to `Assignments` and add the necessary `People` to it.

1. Go to `General` and fill in and make note of the security settings:

   - `APPLICATION`
     - **Allowed grant types**: uncheck `Implicit` option
   - `LOGIN`
     - **Login redirect URIs**
       - `http://localhost:3000/callback/`
       - `https://testokta.suppstaging.royalfloraholland.com/callback`
     - note that this address is _absolute_, i.e., starts with `http(s)://`.
     - **Logout redirect URIs**: `http://localhost:3000/`
     - **Login initiated by**: `App Only`
     - **Initiate login URI**: `http://localhost:3000/`
   - `Client Credentials`
     - **Client ID**: make note of the value in this field, you will need it in `Config.ts` file

1. Go to the `Security | API`:

   - in `Authorization Servers`, choose `rfh-custom`
     - on `Access Policies` add a new `CAI Base React` policy and assign it to clients of `CAI Base React` app
     - create a new `AccessCAIBase` rule as follows:
       - `Users`: `All`
       - `Groups`: `All`
       - `Scopes`: `openid, profile, email`
   - if missing in `Trusted Origins`, click the <kbd>Add Origin</kbd> button and add the URIs you have specified earlier, e.g., `http://localhost:3000`; make sure to put checkmarks by both `CORS` and `Redirect` types:

     ![add cors](images/add_cors.png)

## OKTA Client

### src/common/config/Config.ts

Here you will find the necessary Okta settings in the `oidc` section. The `scopes` parameter should have the same value as in Okta portal (see above).

### src/App.tsx

`App.tsx` is the main entry point of the app. Here we use `Security` element of the `@okta/okta-react` module.

```jsx
<Security {...Config.oidc}>...</Security>
```

### src/common/components/CaiAppBar.tsx

Here we use `useOktaAuth` hook from the `@okta/okta-react` module to handle login and logout:

```jsx
const { oktaAuth } = useOktaAuth()

const handleLogIn = async (): Promise<any> => await oktaAuth.signInWithRedirect('/')

const handleLogOut = (): void => {
  oktaAuth.signOut('/')
  window.location.pathname = '/'
  handleClose()
}
```

### src/common/components/Routes.tsx

Here we make sure only authenticated users can see the app pages. For that we use `SecureRoute` and `LoginCallback` from the `@okta/okta-react` module:

```jsx
<Switch>
  <Route path='/' exact component={HomePage} />
  <Route path='/callback' component={LoginCallback} />
  <SecureRoute path='/abbreviations' exact component={AbbreviationList} />
  <SecureRoute path='/abbreviations/:id' component={AbbreviationDetail} />
  <Route component={NotFound} />
</Switch>
```
