# Development Notes

## Custom TypeScript types

Sometimes you have to import a third-party NPM package which has no type definitions. This can cause TypeScript linting issues which can be easily avoided. Just declare them as modules inside the `src/glolbal.d.ts` file:

```javascript
declare module '@cai/cai-ui'
declare module '@okta/okta-auth-js'
declare module '@okta/okta-react'
```

---

## Routes Component

In `App.tsx` we have placed all dynamics into the `Routes` component. This way, the styling could have been moved completely out of `App.tsx` into the `Routes` component. It appeared that the `BrowserRouter` component could not have been moved to the `Routes` as well and had to stay in `App.tsx` wrapping itself around `Routes`. The reason is that the `useHistory` hook in the Routes's `useEffect` hook needs the global context. That context is created by the `BrowserRouter`.
