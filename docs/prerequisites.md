# Prerequisites

## Node.js

Make sure your version of [Node.js](https://nodejs.org/en/download/) is at least LTS which today is `12+`.

## Global NPM Packages

If you are a project developer, it is recommended to have the following NPM packages installed globally:

```bash
npm i -g browser-sync eslint
```

These packages are quite big and It will save you a lot of development time as npx will use them instead of installing them each time you run the `npx` command. The downside is that once you have them installed globally, it is your responsibility to keep them updated. `npx` always checks and uses the globally installed version first. If it finds none, it runs a one-time on-the-fly installation of the latest version available.

Upgrading them is easy:

```bash
npm update -g
npm upgrade -g
```

You can always check what you have installed globally: `npm list -g --depth=0`.

## Local NPM Packages

Some other small packages like `json-server`, `npm-run-all`, `rimraf`, etc. can be installed with `npm i -D` command inside the project. They also will be used by `npx` without installation. The downside is that they will be increasing the list of your dev dependencies.

## VSCode Extensions

It is **strongly recommended** to have the following VS Code extensions installed in your editor:

- Code Runner
- Debugger for Chrome
- ESLint
- Jest
- Markdown All in One
- Prettier
- Terminal
- TSLint
- Visual Studio IntelliCode

The following VS Code extensions are _nice-to-haves_:

- Auto Import - ES6, TS, JSX, TSX
- nbsp-vscode
- Shell Launcher
- Todo+

## VSCode Settings

### Prettier

Once you have installed the `Prettier` VSCode extension, type `Ctrl`+`,` (on Windows or Linux) or `Cmd`+`,` (on Mac) to open the VSCode settings and open `User | Extensions` menu tree:

![vscode user extensions settings](images/user_extensions.png)

Scroll down to `Prettier` and click on it:

![vscode prettier settings](images/prettier_settings.png)

On the right, work through the `Prettier` settings according to this table (leave missing to their defaults):

| Setting                      |     | Value     |
| ---------------------------- | --- | --------- |
| Arrow Parens                 | :   | avoid     |
| Embedded Language Formatting | :   | auto      |
| Enable                       | :   | checked   |
| End of Line                  | :   | auto      |
| Jsx Single Quote             | :   | checked   |
| Print Width                  | :   | 88        |
| Require Config               | :   | checked   |
| Semi                         | :   | unchecked |
| Single Quote                 | :   | checked   |
| Tab Width                    | :   | 2         |
| Trailing Comma               | :   | es5       |
| Use Editor Config            | :   | checked   |
| Use Tabs                     | :   | unchecked |

### ESLint

Here are a couple of settings for the `ESLint` extension:

| Setting        |     | Value     |
| -------------- | --- | --------- |
| Format: Enable | :   | unchecked |

### TSLint

Here are a couple of settings for the `TSLint` extension:

| Setting   |     | Value     |
| --------- | --- | --------- |
| JS Enable | :   | unchecked |

### Bash terminal

It is advisable to use `Git Bash` terminal as the default terminal inside your VS Code. It comes installed together with VS Code although most probably on Windows `PowerShell` becomes the preferred terminal shell by default. Type `Ctrl` + `` ` `` (backtick quote) or choose `Terminal | New Terminal` from the menu above to open the built-in terminal. In the terminal combo on the right open the terminal menu and click on `Select Default Shell`:

![open terminal menu](images/open_terminal_menu.png)

Choose `Git Bash` from the next menu:

![choose git bash termanal](images/choose_bash.png)

You are done! Reopen the terminal window and you will see the beautiful Git Bash colors:

![git bash prompt](images/git-bash-prompt.png)

The nice thing of this terminal is that it understand most of the Linux shell commands like `ls`, `rm`, `pwd`, `cat`, `touch`, etc. Also, you can see the name of the currently checked out branch (`features/docs` in this case) and its status (that it is "dirty" in this case).

## Git

### Git Config

Run these command in your terminal to make sure that every merge between two branches creates its own commit even if `fast forward` is possible:

```bash
git config --global merge.commit no
git config --global merge.ff no
git config --global pull.ff yes
```

## For Project Administrators

> This section is for the project administrators only. Proceed with caution!

### NPM Updates

It is advisable to keep your NPM dependencies updated. However, this needs to be done carefully by a senior developer and properly tested before committing due to possible breaking changes.

You can check which NPM packages **can** be updated by: `npm run latest`. If there are updates available, change the corresponding versions in `package.json` and run `npm run reinstall`.

> **DON'T UPDATE FIXED VERSIONS**

There are a couple of NPM packages that have a fixed version (no `ˆ` in front). That is because changing them breaks the consistency of the dependencies tree or breaks the code. At time of writing of this document, it were the following three packages:

```json
"dependencies": {
  ...
  "react-hook-form": "5.7.2", // upgrading to v6 breaks the code
  ...
},
"devDependencies": {
  ...
  "webpack": "4.42.0",
  "webpack-dev-server": "3.10.3"
},
```
