## Learn More

- [React documentation](https://reactjs.org/)
- [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started)
  - [GitHub repo](https://github.com/commercetools/merchant-center-application-kit)
- **json-server**
  - [GitHub repo](https://github.com/typicode/json-server)
  - [Zero Code REST With json-server](https://dzone.com/articles/zero-code-rest-with-json-server)
  - [Fake or Mock an API with JSON Server](https://spin.atomicobject.com/2018/10/08/mock-api-json-server/)
- **Internationalization**
  - [react-i18next](https://react.i18next.com/)
  - [i18next](https://www.i18next.com/)
