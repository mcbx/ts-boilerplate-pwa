# We love Docker

## Developing inside a remote container

- install the `Remote - Containers` VS Code extension
- check files inside the `.devcontainer` folder if you are curious
- click on the <kbd>><</kbd> button in the bottom left corner and choose `Remote-Containers: Reopen in Container`
- more information to be found [online](https://code.visualstudio.com/docs/remote/containers)

## Running locally with Docker

You can run the complete PWA in your a Docker environment using the provided Dockerfile. To use:

1. Build the Docker image: `docker build -t voorbeeld-pwa-frontend:local .`
1. Run it: `docker run -it --rm -p 3000:3000 -v $(PWD):/app voorbeeld-pwa-frontend:local`

This will start your container, listening on port 3000 with a bind mount on the complete directory. The
default command will start the HTTP listener and through the bind mount supports hot reloading. Rebuilding
is only required if something changes in the dependencies.

Any of the other commands can also be run inside the container by appending it to the `docker run` command. That way the
CMD from the Dockerfile will be overridden. Examples:

- ESLint: `docker run -it --rm -p 3000:3000 -v $(PWD):/app voorbeeld-pwa-frontend:local npm run lint`
- Prettier: `docker run -it --rm -p 3000:3000 -v $(PWD):/app voorbeeld-pwa-frontend:local npm run prettier`

The container is run with the --rm flag, meaning it's removed once the given command is done. Any files written outside of the bind mount are then gone. Run without the --rm flag to retain the container.
