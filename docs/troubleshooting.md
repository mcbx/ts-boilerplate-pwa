# Troubleshooting

## DEBUG: Error: Cannot find module 'jest.js'

![jest runner error](images/jest_runner_error.png)

## DEBUG: Unverified breakpoint, Breakpoint ignored

If using terminal sessions on WSL (Windows Subsystem for Linux), there is a slight issue here due to the difference in how paths are referenced. Inside WSL the path to the project would be something like `/mnt/c/Users/YourUserName/dev/project/` but this won't resolve to a Window path in VS Code (i.e., `C:\Users\YourUserName\dev\project`).

As a result, you may get the following error in VSCode when running the debugger:

```none
Unverified breakpoint, Breakpoint ignored because generated code not found (source map problem?).
```

Essentially this means that VSCode is not able to map the source files listed in Chrome to actual source files on the disk. To get around this configure `sourceMapPathOverrides` setting in `launch.json` to essentially alias the Unix paths to the Windows paths:

```json
{
  "version": "0.2.0",
  "configurations": [{
    ...
    "sourceMapPathOverrides": {
      "/mnt/c/the/actual/path/to/src/*": "${workspaceFolder}/src/*"
    }
  }]
}
```

## ESLint couldn't find the config "prettier" to extend from

When running linting you see the following error message:

```none
ESLint couldn't find the config "prettier" to extend from. Please check that the name of the config is correct.

The config "prettier" was referenced from the config file in "C:\src\fh-vvn-okt-rct\.eslintrc.js".
```

Try force reinstalling everything:

```bash
npm run reinstall
```

## Invalid or unexpected token

![Invalid or unexpected token](images/jest_invalid_token.png)

This is a consequence of Jest being unable to handle non-JS/TS assets that are being imported inside the JS/TS imports in the testfiles.

I have added two things ([tip of mbaranovski](https://www.bountysource.com/issues/41190983-syntax-error-invalid-or-unexpected-token-with-png)):

1. in `jest.config.js`:

   ```js
   moduleNameMapper: {
     ...
     '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
       '<rootDir>/assetsTransformer.js',
     '\\.(css|less|scss)$': '<rootDir>/assetsTransformer.js',
   },
   ```

2. new file `assetsTransformer.js`:

   ```js
   const path = require('path')

   module.exports = {
     process(src, filename, config, options) {
       return 'module.exports = ' + JSON.stringify(path.basename(filename)) + ';'
     },
   }
   ```

## HUSKY + LINT-STAGING

When trying to commit, you see an error `The file does not match your project config`.

```none
× eslint --fix found some errors. Please fix them and try committing again.

C:\src\fh-vvn-okt-rct\SrcReact\fh-vvn-okt-rct\jest.config.js
0:0  error  Parsing error: "parserOptions.project" has been set for @typescript-eslint/parser.
The file does not match your project config: C:\src\fh-vvn-okt-rct\SrcReact\fh-vvn-okt-rct\jest.config.js.
The file must be included in at least one of the projects provided
```

This means that - in this case - `jest.config.js` file is not included in the `include` section of `tsconfig.json`. It needs to be included:

```json
"include": ["src", "jest.config.js"]
```

## JEST: Start jest from non-root folder

If you don't use the root of your project for your JS with Jest tests, do not worry, you can still use this project. You will need to use the `Start Jest Runner` command, and maybe have to configure your own `jest.pathToJest` setting inside the `.vscode/settings.json` to whatever you would use.

These are the activation events which trigger the runner to start:

```json
"activationEvents": [
  "workspaceContains:node_modules/.bin/jest",
  "workspaceContains:node_modules/react-scripts/node_modules/.bin/jest",
  "workspaceContains:node_modules/react-native-scripts",
  "onCommand:io.orta.jest.start"
],
```

These are the things that will trigger the extension loading. If one of these applies, and you're not seeing the `Jest` in the bottom bar, reference the self-diagnosis at the [Jest VS Code Troubleshooting Page](https://github.com/jest-community/vscode-jest/blob/master/README.md#troubleshooting).

## JEST: SyntaxError: Unexpected token export

Add this Jest configuration to `jest.config.js` file:

```js
transformIgnorePatterns: ['/!node_modules/(?!(@floriday)/)/'],
```

## Parsing error: "parserOptions.project"

![Parsing error: "parserOptions.project"](images/parsing_error.png)

If this is a help file, add the missing file to `.eslintignore` so that it is ignored during linting. Otherwise to `.eslintrc.js`.

## Patch for package @types/mui-datatables error

Reinstalling all packages on `12-10-2019`:

![patch-package error](images/patch-package-error.png)

<!-- TODO AR: Vuk navragen over deze patch -->
