# Debugging in VS Code

## Launch.json

In order to debug in VS Code via Chrome, you need to install the _Debugger for Chrome_ extension. It supports the following features:

- Setting breakpoints, including within source files when source maps are enabled
- TypeScript, via source maps
- Stepping, including using the buttons on the Chrome page
- Locals scope variables via the VS Code Locals pane
- Debugging eval scripts, script tags, and scripts that are added dynamically
- Watches via the VS Code Watch panel.
- The debug console
- Most console APIs

If you want to debug in VS Code via Firefox, install _Debugger for Firefox_ extension as well.
