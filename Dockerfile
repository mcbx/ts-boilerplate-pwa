FROM node:15.12.0-buster-slim

COPY . /app

WORKDIR /app

RUN npm -g install

EXPOSE 3000

CMD [ "npm", "run", "start-http" ]
