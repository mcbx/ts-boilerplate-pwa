# PWA Frontend Documentation

## Table of Contents

- [Home ⇗](/)
- [Prerequisites⇗](/docs/prerequisites.md)
- [Internationalization ⇗](/docs/internationalization.md)
- [Code Quality ⇗](/docs/code-quality.md)
- [Testing ⇗](/docs/testing.md)
- [Debugging ⇗](/docs/debugging.md)
- [Mock API ⇗](/docs/mock-api.md)
- [Development Notes ⇗](/docs/notes.md)
- [Troubleshooting ⇗](/docs/troubleshooting.md)
- [References ⇗](/docs/references.md)

> This documentation has a WIP (Work in Progress) status. As a team member please feel free to add to it or finetune it.

## Getting started

If you are a new developer to this project, make sure to have all the necessary [prerequisites](/docs/prerequisites.md) in place before you start working with it.

## Important

> To run the documentation website use `npm run docs` command after you install all npm packages. You don't have to but it is strongly advised to have `browser-sync` package installed globally:
>
> ```bash
> npm i -g browser-sync
> ```

> Mock back-end uses `restore` script to restore the mock database. Consider to have `copyfiles` package installed inside your project:
>
> ```bash
> npm i -D copyfiles
> ```

> Install the project:
>
> ```bash
> npm i
> ```

> Run the application:
>
> ```bash
> npm run start
> ```

> Run the application with the mock API (local backend for the Abbreviations view):
>
> ```bash
> npm run start:mock
> ```

## More Information

You can find some relevant links in the [References](/docs/references.md) section. Also, there is [Azure DevOps Project Wiki](https://dev.azure.com/royalfloraholland/Training/_wiki/wikis/Training.wiki/767/React-Base-Template-Wiki) which you may be useful.
