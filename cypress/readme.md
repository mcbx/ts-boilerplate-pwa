# cypress

when starting cypres in a folder it will automatically make the folder cypress if not already there.

## global settings

located in cypress.json created in the root of the project you can for example set the base url.
ther settings such as what test files to include and other settings.
https://docs.cypress.io/guides/references/configuration.html#Folders-Files

## Components

In the components folder you can describe the actions you can do on the components and certain assertions.

## Pages

You can describe the standard procedures that you execute on the pages.
