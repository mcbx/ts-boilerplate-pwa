describe('Navigate to Abbreviations', () => {
  it('Visit page and validate', () => {
    cy.visit('/')
    cy.get('[type="button"]').contains('Afkortingen').click()
    cy.contains('Afkorting')
  })
})

describe('Navigate to Home', () => {
  it('Visit page and validate', () => {
    cy.get('[type="button"]').contains('home').click()
    cy.contains('Applicatie')
  })
})

describe('Navigate to Abbrev list with navbar', () => {
  it('Visit page and validate', () => {
    cy.get(':nth-child(2) > .MuiBottomNavigationAction-wrapper').click()
    cy.get('.MuiInputBase-root')
  })
})
