describe('Abbreviations List Page', () => {
  it('Go to abbreviations view and validate detail view', () => {
    cy.visit('/abbreviations')
    cy.contains('afv').click()
    cy.contains('Details')
    cy.get('input[id="description"]').should('have.value', 'afvoer')
    cy.get('input[name="englishDescription"]').should('have.value', 'delivery')
    cy.get('input[id="createdBy"]').should('have.value', 'es')
    cy.get('input[name="description2"]').type('dit is omschrijving 2')
    cy.get('input[name="description2"]').should('have.value', 'dit is omschrijving 2')
  })
})

describe('Navigate to Abbreviations List', () => {
  it('Visit page and validate', () => {
    cy.get('[type="button"]').contains('Afkortingen').click()
    cy.contains('Afkorting')
  })
})
