describe('Go to Home with RFH Icon button', () => {
  it('Visit page', () => {
    cy.visit('/')
    cy.get('.makeStyles-logo-9 > .MuiIconButton-label > .MuiSvgIcon-root').click()
    cy.contains('Applicatie')
  })
})
