describe('Abbreviations List Page', () => {
  it('Go to abbreviations view and validate', () => {
    cy.visit('/abbreviations')
    cy.get('tbody > tr').should('have.length', 45)
    cy.get('td').should('be.visible')
    cy.contains('aft')
  })
})

describe('Test search function', function () {
  it('Type btw in searchbar and validate result', () => {
    cy.get('#searchText').type('btw')
    cy.get('tbody > tr').should('have.length', 1)
    cy.contains('V.A.T.')
  })
})
