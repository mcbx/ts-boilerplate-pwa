describe('Change translation to english', () => {
  it('Visit page', () => {
    cy.visit('/')
    cy.get('.makeStyles-menuIcon-8').click()
    cy.get(
      ':nth-child(1) > .MuiButtonBase-root > .MuiListItemText-root > .MuiTypography-root'
    ).click()
    cy.contains('Application')
    cy.contains('Abbreviations')
  })
})

describe('Change translation to dutch', () => {
  it('Visit page', () => {
    cy.get(
      ':nth-child(1) > .MuiButtonBase-root > .MuiListItemText-root > .MuiTypography-root'
    ).click()
    cy.contains('Applicatie')
    cy.contains('Afkortingen')
  })
})
