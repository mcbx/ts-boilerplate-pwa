// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
import { createYield } from 'typescript'

// before(() => {
//     // create alias of the testUser
//     cy.log('Create alias testUser')
//     cy.fixture('users/test_user.json').as('testUser')
// })

// Mock okta
// Cypress.Commands.add('mockOktaLoggedState', () => {
//     const oneDayFromNow = Date.now() + 1000 * 60 * 60 * 24
//     cy.fixture('mock/okta_access_token.json').then(token => {
//       token = JSON.stringify(token)
//       token = token.replace('"oneDayFromNow"', oneDayFromNow)
//       token = token.replace(/\s/g, '')
//       localStorage.setItem('okta-token-storage', token)
//     })
// })

// -- This is a parent command --
// Cypress.Commands.add('login', () => {
    // programmatically log us in without needing the UI

//   cy.visit('/abbreviations')
  //   cy.fixture('users/mockuser.json').then(content => {
  //     cy.get('#account').type(content.account)
  //     cy.get('#password').type(content.password)
  //   })

//   cy.get('@mockuser').then(user => {
//     cy.request('POST', '/login', {
//         account: user.account,
//         password: user.password
//         }
//     ).then((resp) => {
//         cy.log(resp)
//     })
//   }

//   cy.get('[data-cy="submitlogin"]').click()
// })

// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
