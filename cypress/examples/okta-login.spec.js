import { createYield } from 'typescript'

before(() => {
    // create alias of the testUser
    cy.log('Create alias testUser')
    cy.fixture('users/test_user.json').as('testUser')
})

// describe('LOGIN RFH DOMAIN', () => {
//     it('Visit page rfh and log-in', () => {

           //eslint-disable-next-line max-len
//         cy.visit('https://sts-sys.floraholland.nl/adfs/ls/?SAMLRequest=FillInYourToken')

//         cy.get('[id="ctl00_ContentPlaceHolder1_UsernameTextBox"]').type('sy018503')
//         cy.get('[id="ctl00_ContentPlaceHolder1_PasswordTextBox"]').type('test0000')
//         cy.get('[id="ctl00_ContentPlaceHolder1_SubmitButton"]').click()

        // cy.get('@testUser').then(user => {
        //     cy.request('POST', '/oktaLoginUrl', {
        //         account: user.account,
        //         password: user.password
        //         }
        //     ).then((resp) => {
        //         cy.log(resp)
        //     })
        // })

//     })
// })

describe('GO TO APP DOMAIN', () => {
    it('Go to authenticated page', () => {
        cy.visit('https://localhost:3000')
        cy.get('[type="button"]').contains('Abbreviations').click()
    })
})
